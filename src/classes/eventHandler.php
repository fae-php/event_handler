<?php

/**
 * FAE 
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2020 Callum Smith
 */

namespace FAE\event_handler;

use FAE\fae\fae;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class eventHandler
{
  // @var eventHandler Singleton instance
  static $handler;

  // @var EventDispatcher Dispatcher instance
  var $dispatcher;

  public function __construct()
  {
    $this->dispatcher = new EventDispatcher();
    $this->generateSubscribers();
  }

  public function generateSubscribers()
  {
    $subscribers = fae::loadOptions('event_handler');
    if (is_array($subscribers)) {
      foreach ($subscribers as $className) {
        $this->addSubscriber(new $className);
      }
    }
  }

  public function addSubscriber(EventSubscriberInterface $subscriber): void
  {
    $this->dispatcher->addSubscriber($subscriber);
  }

  /**
   * Get the event dispatcher
   *
   * @return EventDispatcher
   */
  public function getDispatcher(): EventDispatcher
  {
    return $this->dispatcher;
  }

  /**
   * Singleton function
   *
   * @return self
   */
  public static function getHandler(): self
  {
    if (!self::$handler) {
      self::$handler = new self();
    }

    return self::$handler;
  }
}
